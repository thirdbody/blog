---
title: Who am I?
date: 2020-07-06 12:12:06
tags:
- Personal
---

My name is Milo Jonathan Gilad, and I am an Israeli-American Computer Science and Cybersecurity student at the University of Central Florida.

I previously attended North Broward Preparatory School in Coconut Creek, Florida for high school, and the University of Maryland (College Park) for the Summer and Fall 2020 semesters of my undergraduate degree.

I plan to write about projects I'm working on (or have worked on in the past), and miscellaneous pieces of advice that my past self would have appreciated.

If you're interested in learning more about me, please see my profile website at [www.milogilad.com](https://www.milogilad.com).
