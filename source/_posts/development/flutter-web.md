---
title: 'Flutter Web, or How to Get a Back-End Dev to do Front-End Work'
date: 2020-07-06 14:10:38
tags:
- Software
- Web
- Development
- Flutter
---

I was introducted to Flutter in a period of my life where I was experiencing mobile development *ennui*. The mechanisms for UI development shown to me in Xcode's Storyboards were clunky and dissatisfying, and the brand new Swift UI framework was too new to account for every possible use case, forcing me to create iOS UIs with a Frankensteinian combination of UI-builder generated code and explicitly programmed scenes.

Flutter showed me what the future of mobile development could look like. The "widget" model, resembling a purely-programmatic implementation of Xcode's Storyboards, was an absolute delight from the start. The excellent integration with Visual Studio Code (especially the autocompletion) and more documentation than I've written in my entire life sealed the deal. Every time I've developed a mobile app since then, I've done it in Flutter.

Flutter Web only became relevant to me a few weeks ago, when I was approached by a friend with a request to develop a [secure contract exchange system](https://gitlab.com/thirdbody/contrax) as an alternative to email. I gladly obliged, developing my database skills and reuniting with Golang for the [backend](https://gitlab.com/thirdbody/contrax_backend).

That didn't change the fact that I had been asked for a website. Not a command-line utility, not a mobile app, but something accessible by web browser. I turned my attention at first to the various frameworks which have littered the web development expanse for ages - Angular, React, Vue and the likes. And then I remembered my good friend Flutter.

## What makes Flutter Web so special?

It's the same Flutter you use to design mobile app UIs... but for the web.

It works in an almost identical manner. You construct widgets (analogous to "scenes") which give way to other widgets. They can do things like collect user input, display animations, and all the other things you would expect from your typical mobile application.

I said that it works in an *almost* identical manner because there is one crucial difference: Flutter Web can access the underlying HTML in much the sameway that JavaScript can. You can facilitate file uploads, page refreshes, and HREF navigation directly from within your Flutter codebase.

## Is it ready for production use?

Not really.

Flutter Web is only available on the `beta` channel, which will ideally be a temporary arrangement while it is continuously worked on and refined.

You can enable the functionality for it yourself, by using the below commands:

```bash
flutter channel beta
flutter upgrade
flutter config --enable-web
```

There's no guarantee that existing projects will be compatible with the web functionality, but you can enable it within a project using [this guide](https://flutter.dev/docs/get-started/web#add-web-support-to-an-existing-app).

A relatively surefire way to make a Flutter Web project ready for production use is to design it from scratch as a web application. This is completely against the vision of the Flutter framework (write one codebase, deploy it everywhere) but until web support moves to the `stable` channel you can't be sure there's not a bug lurking deep within your codebase.

## Is it promising?

Very.

I am a backend developer, which means that I vastly prefer the logic and frameworks powering APIs and servers in comparison to the logic and frameworks powering the websites that you actually see with your own eyes.

If I, an ardent hater of all things front-end, can be made to enjoy web development with this framework, then it is *very* promising.

It is dead simple to get started with, highly intuitive, and (with practice) almost natural. I can start a new Flutter project and go 10 minutes without needing to look at documentation, which is not something I can say for Angular or another of those frameworks (I cannot actually start a project in those without looking at the documentation).

If development on Flutter Web keeps progressing as it has recently, and it moves without trouble into the `stable` channel, I am confident saying that front-end web development will become vastly more simple (at least, for the average back-end developer).
