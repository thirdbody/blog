---
title: 'Framework Overload: Do Things the Simple Way, Not the Right Way'
date: 2020-07-07 11:41:11
tags:
- Software
- Development
- Strategy
---

Lying just beyond the introductory self-guided resources to a language is a vast sea of various solutions built on top of it.

The JavaScript ecosystem is the most notorious for this framework overload - so much so that JS developers often joke that every 5 minutes a new framework is born - but it occurs in every major language. There are tools for dependency managment (i.e. Haskell's stack vs. cabal vs. Nix issue), alternative syntaxes (i.e. JavaScript's vanilla vs. TypeScript vs. CoffeeScript vs. PureScript vs. Dart issue), and boilerplate-reducing utilities (i.e. Angular vs. Vue vs. React vs...).

These frameworks more often than not are solutions in search of a problem. The oversaturation of JavaScript as a programming language makes it particularly susceptible to this phenomena.

## Which framework do I use?

As a rule of thumb, if five minute's research cannot tell you which one is right for you, *then the answer is none at all*.

If five minutes can't net you an obvious answer, it's likely because the project you seek to make a reality is unique. That's not a bad thing, it just means that any framework you choose will force you to modify your project in some way to comply with the protocols set forth by that framework.

"Force" is a strange word to use here because many of these frameworks espouse interoperability with existing codebases. Those same frameworks also tend to have an established set of "best practices" that, when followed, guarantee that the project will compile/build successfully. Without following those best practices, of course, the project offers no guarantees as to the success of your build. So while many of these frameworks will not force you to comply with their standards, they will encourage one set of practices while discouraging other kinds of practices.

This is not necessarily a bad thing so long as your needs line up with the framework's objectives. If you can't find a framework that does this within 5 minutes, odds are that it doesn't exist.

## Large Projects

Projects that are beyond more than a couple of weeks in development, or that will need to be maintained for a long time, often benefit more from frameworks than short-term projects. That deeper initial investment associated with learning how the framework operates pays off more if your project is continuously developed over a longer period of time.

But how do we reconcile the 5-minute rule with this obvious larger-project advantage? We do so by migrating mid-development.

As your project takes digital form and exits the realm of your imagination it will be refined. This refinement process, more often than not, includes bringing your code in line with industry best practices. As your code gets closer and closer to the industry norm, it will also get closer and closer to the best practices and standards laid forth by the frameworks.

In other words, it is quite likely that the more you develop your project, the more easily it will fit within the limitations of a framework. It won't necessarily become less unique, but the codebase will take on a form that more closely resembles the "average" codebase envisioned by the developers of these frameworks.

## Shifting Strategies Mid-Development

Depending on how loosely you define "mid-development," you can commence the process of shifting your strategy as your program enters the middle stage of development (where the main features have been created and you are about to start smoothing out the rough edges) or as your program leaves the middle stage and enters the final stage prior to the production release.

Starting the shift too early can make development more difficult down the line, since you'll be operating within the limitations of the framework (which you may come to regret, if a feature you wish to develop is egregiously outside the scope of that framework). Do it too late, and the task of migrating the codebase becomes monumental. How do you find the right moment to shift over?

The advice I have in this respect is to start *later rather than sooner*. It is very annoying to find and replace references, rename file extensions, and whatever else a framework may demand; but it is far more devastating to be forced to shift to a different framework or even to abandon the framework entirely because you picked too early before you knew the entire demands of your project. Of course, in a perfect world, every developer knows the entire demands of their project prior to commencing work on it. But we do not live in a perfect world, and there are no perfect developers.

## Hack together your own framework

Frameworks do not have to be an "all-or-nothing" ordeal. Your limits in terms of the frameworks that you can use are entirely determined by your creativity.

An old project of mine was initially developed with Webpack and JS, but in the middle of development I became hooked on TypeScript. I proceeded to convert every single JS file to a strictly-typed TS one, and developed a `Makefile` that compiled the TS into JS first before then sending Webpack to minify the JS for production deployment (and yes, I know that Webpack has TS support, but that support was at the time strict and required the TS to obey specific parameters that I had no interest in complying with).

GNU's make utility is probably your best bet at merging many frameworks to create your own Frankenstein's framework. Any scripting utility works; make is simply the most intuitive (to me, a Linux user, since just about every Linux machine has make installed but not always python or perl). 

The moral of the story is that you are the developer. Not the person who designed the framework, not the person who wrote about industry best standards on the Internet, you. Pave your own way so you can make the project you want instead of the project the framework wants. But do so intelligently, to avoid making your project so cumbersome that to enter the codebase is to enter a labyrinth of your own creation.
