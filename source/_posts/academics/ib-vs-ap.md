---
title: 'IB vs. AP: What I Learned in High School'
date: 2020-07-06 12:37:19
tags:
- North Broward Preparatory School
- High School
- Academics
- College Credit
---

High school students who wish to challenge themselves with their course material, earn college credit in advance, or both, have two main options: Advanced Placement (AP) and International Baccalaureate Diploma Programme (IBDP).

My high school experience has led me to conclude that, for my purposes, one of these programs was vastly superior to the other. Before I tell you which one, however, I want you to understand how I came to my conclusion.

## AP

Advanced Placement is managed by the College Board, a U. S. based non-profit organization which also manages the School Admission Test (SAT) and Test of English as a Foreign Language (TOEFL).

AP courses generally follow the structure of a traditional U. S. college/university course. Taught out of a textbook designed for the purposes of AP course instruction, the objective of the course is to cover all the material which can be present in the end-of-year examination.

That end-of-year exam takes place in early or mid-May, and lasts anywhere from 1 to 3 hours. It contains a single multiple-choice section, and anywhere from 1 to 3 writing sections. Performance on that exam determines your AP Score, which is on a scale of 1 to 5 (mimicking the U. S. letter grading scale, with 5 representing an "A" and 1 representing an "F"). 3 and above is considered a passing grade, and most colleges/universities in the U. S. will award credit for a 3 or above.

## IBDP

The International Baccalaureate Diploma Programme is managed by the International Baccalaureate, a Geneva non-profit foundation which also manages the Career-related and Middle Years Programme.

IB courses follow a less traditional educational approach when compared to AP. IB courses are heavily project based, with in-class portfolios and presentations making up the vast majority of a student's final grade in the course. Depending on the particular course, end-of-year exams comprise some percentage of the final grade while these in-year projects comprise the rest.

Notably, IB courses (with few exceptions) last for 2 academic years, with projects split between the two years and the final exams taking place at the end of the second year.

The received score is on a scale of 7, with most colleges/universities accepting a 5 or above.

## Why is AP better for the American student?

While the IBDP syllabi and project-based educational standards are vastly more engaging (and, perhaps, more informative) than the rote memorization encouraged by AP, the American student is vastly better off if they shun the IB and take AP courses in their place.

From what I've been told about what happens behind the scenes, it's not a difficult decision to make. The IBDP contains fundamental flaws within its implementation, stemming from their approach to grading coursework.

For example, rather than assessing the submitted coursework of every student, the IB marks only a small sample of a class's submitted coursework. Then, based on how much the teacher's marks deviated from the IB's marks, the IB proceeds to reduce the marks of every unassessed student's coursework by the difference between the IB's marks and the teacher's marks. Occasionally, this results in a class's grades increasing, in the event that the teacher was a harsher grader than the IB; but in the majority of cases every student's grade is reduced.

The practice of treating each class as a single entity fails to address each student's individual performance, and penalizes students who may have achieved far better results than the rest of their class. This is especially true for classes taught by lenient instructors - if most students in a class receive good scores on their projects, and the IB samples the worst projects of the class, then students who may actually have deserved high marks will be severely penalized based on the performance of the worst students in the class.

This tactic is as efficient as it is misrepresentative - applying the performance of a small sample of students to an entire class saves hours of grading work for each class. It may very well be necessary for the IB to take this shortcut - grading several different kinds of projects (and the final exam) based on different markschemes is a far more laborious task than grading a single AP exam (half of which is graded automatically since it is multiple-choice). 

Regardless of its necessity, the employment of this tactic in determining a student's final grade means that a student has little control over their academic performance. In comparison, the AP (which relies on a single exam which is individually graded by two separate persons) affords far greater control over a student's final results.

An additional advantage of the AP is that its final exam can be administered independently of a course. If a student is already proficient in Chinese, Biology, or any number of the subjects administered by the AP, they can purchase the exam for that subject through a school which participates in the AP. They can then sit for that exam and receive a score just as they would if they had taken the course, only they do not waste time by re-learing material that they are already fluent in. This is impossible with the IBDP, which requires coursework to be submitted through a teacher as part of a class. This independent exam administration also means that AP exams can be taken more than once.

Another aspect of the AP to note is that it lasts a reasonable length of time: 1 academic year. Receiving a poor score in AP is less of a waste than receiving a poor score in IB, since less time was wasted in receiving the undesired score.

An international student, or a student seeking admission to an international university, may find that this advice is not suited to their needs. International schools have specific protocols in place for students who participate in the IBDP - specifically those who are expected to receive an IB Diploma (which is a program consisting of several IBDP courses + additional project work). But students seeking admission to American schools will find few (if any) that place significant weight on a student's receipt of the IB Diploma. Those schools are often more concerned with the rigor of the courses in a student's transcript.

## Advice for NBPS Students

North Broward has been steadily replacing its roster of AP courses with their IBDP equivalents.

This is understandable - the school is increasingly gearing itself for the international market, through the development of its boarding school aspects (i.e. construction of new dormitories). To achieve an internationalized image, the school must also offer internationally-recognized programs, and offering these alongside American equivalents will reduce enrollment for both and strain resources.

That doesn't mean you're powerless to place your education within your own hands. Historically, the school has permitted IB students to register for their equivalent AP exams each year (although they must pay for the exam themselves). This is quite likely your best option as a student seeking American school admission.

In the event that no course/exam is offered at North Broward for an AP program that you'd like to participate in, you can purchase a self-study textbook and contact nearby schools (i.e. Monarch) to purchase AP exam seating through them. This will mean, however, that you'll have to go to another school to take those exams.

So long as you develop a comprehensive academic plan regarding which AP and/or IB courses and exams you'll take, you should be able to start your college/university experience with a definitive head-start. Avoiding entry-level classes such as English 101 can make your transition from high school to college a much smoother one.
