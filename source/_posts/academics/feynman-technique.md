---
title: 'The Feynman Technique: Learning How to Learn'
date: 2020-07-06 22:24:07
tags:
- High School
- Academics
- College
- Education
---

As our understanding of various subjects increases, so too does their complexity and obscurity to the uninitiated. Physics and mathematics, biology and ecology, computer science and programming - these are all subjects which an average person will shy from out of the sheer complexity of understanding them.

The consequences of this phenomena are dire. The outsider looking into these subjects may be completely put off from delving into the subject. The child who wishes to grow up and be an astronaut may give up their dreams not because they find better interests elsewhere, but because the complexity of astrophysics scares them away. Increasingly in today's society we are feeling the effects of overspecialization and the death of polymathism - the over-specific knowledge possessed by today's experts prevents them from joining multiple fields of study and advancing both in ways neither could achieve alone.

But what about these subjects makes them so convoluted and unappealing to the novice?

## First Impressions

Everything about a person's perception of a field of study is how they are introduced to it.

More often than not a person is introduced to a "complex" field - think mathematics or physics - as something uniquely difficult to pursue. Sometimes that happens when a person is just a child, making simple inquiries about the world around them.

Why are people introduced to these subjects in ways that reduce their will to learn about it? I think it's because of our negativity bias - we tend to remember the most negative aspects of a thing more than the positive aspects. We remember the very difficult math - the sort taught to us by bad teachers - more than the easy math we were taught by good teachers. The crux of the issue is that the "complex" topics are those which are routinely taught poorly.

Why are these subjects taught poorly? That is a more complex issue, but the most relevant answer is that teachers of these subjects do not know how to teach them properly.

Classes such as history, English, and the other liberal arts are vastly different from the sciences in that none of them require a shift in a person's logical thought processes. For example, the study of English grammar is not understood by the mind in the same way as algebra.

And yet in the majority of cases it seems that these two disparate categories - the liberal arts and the sciences - are taught in nearly identical ways. We cannot  teach two different subjects in the same way and anticipate the same learning response.

## How do we teach the sciences?

We should teach the sciences scientifically.

1. Do not operate on assumptions.
2. Since the first rule means that you can't assume your students have prior knowledge, explain everything in sufficient and clear detail.
3. Teach by the discovery principle - allow your students to form the connections between a theoretical problem and a solution on their own (with guidance).

Let me elaborate on those three rules.

The first rule is simple enough: assume nothing. For example, for your first lecture on calculus, do not assume that your students know what a limit is. Do not assume that they know what a tangent or secant line is, don't assume they know what a slope is, don't assume they know what a line is, and don't assume they know what a function is.

At first glance this first rule seems cumbersome, but it is actually the most important of the three. When you stop assuming things about your students' knowledge, you gain the incredible ability to correct deep-seated misconceptions they may hold about even the most basic principles of a subject. Your students may not have known what a tangent line or secant line was until you explained it. More importantly, students may have only had a *practical* understanding of the concepts that you explain rather than a *conceptual* understanding.

Bad teachers often do not properly explain to students *why* something exists in a subject. Those who fare well under bad teachers do so by developing a practical understanding of the concept - often through the memorization of formulas, acronyms, mnemonic devices, and other tools designed to be applied to specific situations. These students will score highly on the average math placement test not because they actually understand the material, but because they understand how to use it in very specific situations. It is indeed possible to use the material of a subject without understanding it completely, because of the rote nature of test questions (even complex word problems tend to have a discernable pattern which is then associated in the student's mind with a very specific approach). Scores on these tests will fall drastically if students are ever asked to synthesize knowledge based on a conceptual understanding of the material.

An example of this is a student who knows (via the power-rule) how to calculate the derivative of a function, but does not actually understand what the derivative means or the basis for the shortcut which the student used.

The second rule is the embodiment of (and is subject to) common sense. If you assume nothing about a student's prior knowledge, you obviously need to explain everything that you assumed they knew. The question becomes "How much?"

Common sense is the only suitable tool here. When explaining the concept of the derivative - the slope of a tangent line - you must naturally explain the concept of the mathematical limit. It is not, however, advisable to explain things such as one-sided limits, or limits going to infinity, or limits of sequences. These things are not necessary to the problem at hand, which is simply "how do you calculate the slope of a tangent line?" They may be necessary on a separate lesson focused entirely on limits and their applications, but applying common sense to this particular lecture should lead you to only explain the things that are directly relevant to the issue at hand.

It is better for me to show you an example of the third rule than to exhaustively explain it. I wrote an article on [taking the slope of a tangent line](https://encyclopedia.milogilad.com/math/calculus/slope-of-a-tangent-line.html) that followed this "by discovery" approach, which was inspired by Douglas Downing's "Calculus: The Easy Way" (formerly "Calculus by Discovery, " a much more fitting title in my opinion).

Educating students by having them discover the concepts for themselves is a deeply engaging and effective means to teach them. People who actively employ this approach are forced to confront gaps in their knowledge which would otherwise be obscured by buzzwords (since the rules force them to define and explain these buzzwords) and as a result build a comprehensive understanding of the subject matter.

This is the essence of the Feynman Technique. Express information concisely and simply and you will understand it. Hide behind cumbersome language and gaps in understanding and you will not understand it.

## Applying the Feynman Technique to Studying

The principles that apply to educators apply similarly to students. If you, the student, cannot follow the 3 rules of scientific education I've laid out, then you don't understand what you've learned (and perhaps you haven't learned it at all).

A thought experiment: can you explain the concept to a child? Can you strip away the scientific argot and get to the meat of the matter? If you can't then you don't understand it!

The first step towards fixing this is to figure out what you don't know. Examine your own understanding of the topic. Make a list of the things that you *do* know and another list of the things that you *don't* know. Ask yourself to define each and every thing directly related to the topic. Figure out what (lack of) information you're hiding from yourself, and fix it rather than feeling insecure about it.

How do you fix it? You ask questions. Ask Google, your classmates, your teacher. Asking questions becomes easier the more you do it - so make a point of doing it more. Don't wait to patch up holes in your knowledge - fix the issue right away.

After you've fixed the gaps in your knowledge, the rest is simply working towards expressing your knowledge in a succinct way, using the other two rules.

## Concluding Remarks

Do your best to throw away everything you think you know about the difficulty of broaching new subjects. You can broach any subject of any kind - so long as you're not afraid to confront your own lack of understanding, and are willing to work towards creating it.
