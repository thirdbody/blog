.PHONY: build

build:
	@npm install
	@npx hexo generate
	@cp -r ./blog_out ../../public/blog

clean:
	@rm -rf ./blog_out

serve:
	@npx hexo server
